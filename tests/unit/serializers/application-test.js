import { module, test } from 'qunit';
import { setupTest } from 'rock-paper-scissors/tests/helpers';

module('Unit | Serializer | application', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('application');

    assert.ok(serializer, 'Serializer loaded');
  });

  test('it serializes records', function(assert) {
    let store = this.owner.lookup('service:store');
    let record = store.createRecord('user', {});

    let serializedRecord = record.serialize();

    assert.ok(serializedRecord, 'Record serialized');
  });
});
