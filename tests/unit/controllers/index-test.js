import { module, test } from 'qunit';
import { setupTest } from 'rock-paper-scissors/tests/helpers';
import { isEmpty } from '@ember/utils'

module('Unit | Controller | index', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    const controller = this.owner.lookup('controller:index');
    assert.ok(controller, 'Index controller loaded');
  });

  // Replace this with your real tests.
  test('Join user', async function(assert) {
    let store = this.owner.lookup('service:router');
    let indexController = this.owner.lookup('controller:index');
    let model = indexController.join('Test name', true);

    await model;
    assert.ok(model._result !== 'Join failed', 'User created or loaded');
  });
});
