import { module, test } from 'qunit';
import { setupTest } from 'rock-paper-scissors/tests/helpers';

module('Unit | Controller | game', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    const controller = this.owner.lookup('controller:game');
    assert.ok(controller, 'Game controller loaded');
  });

  test('check lastBotPlay', async function(assert) {
    let controller = this.owner.lookup('controller:game');
    assert.equal(controller.lastBotPlay, null);
  });

  test('check normal game options', async function(assert) {
    let controller = this.owner.lookup('controller:game');
    assert.equal(controller.normalModeOptions.length, 3);
  });

  test('check full game options', async function(assert) {
    let controller = this.owner.lookup('controller:game');
    assert.equal(controller.fullModeOptions.length, 5);
  });


  test('Play rock', function(assert) {
    let controller = this.owner.lookup('controller:game');
    let store = this.owner.lookup('service:store');
    controller.model= {};
    controller.model.firstObject=store.createRecord('user', {
        name: 'Test Name',
        score: 0,
        normalMode: true
      });
    let result = controller.play('rock', true);
    assert.equal(result, 0,'Play done');
  });

  test('Update Score', function(assert) {
    let controller = this.owner.lookup('controller:game');
    let store = this.owner.lookup('service:store');
    controller.model= {};
    controller.model.firstObject=store.createRecord('user', {
        name: 'Test Name',
        score: 0,
        normalMode: true
      });
    let result = controller.updateScore(true);
    assert.equal(result, 'Score Updated','Score Updated');
  });

});
