import { module, test } from 'qunit';
import { setupTest } from 'rock-paper-scissors/tests/helpers';
import { isEmpty } from '@ember/utils'

module('Unit | Model | user', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('user', {name: 'Test Name', score: 0, normalMode: true});
    const modelAttributes = !isEmpty(model.name) && !isEmpty(model.name) && !isEmpty(model.normalMode)
    assert.ok(modelAttributes, 'User created with all attributes');
  });
});
