import Controller from '@ember/controller';
import { action, computed } from '@ember/object';
import { service } from "@ember/service";
import { get, set } from '@ember/object'
import { isEmpty } from '@ember/utils'

const NOTIFICATION_OPTIONS = {
  autoClear: true,
  clearDuration: 3000,
  htmlContent: true,
};

export default class IndexController extends Controller {
  @service store;
  @service router;
  @service notifications;

  userName = '';

  // Check if users exists, and create one if not
  @action
  join(userName, isTest) {
    if (!isEmpty(userName)) {
      // Search user
      return this.store.query('user', {
        filter: {
          name: userName
        }
      }).then(user => {
        let userModel = user;
        if (user.length === 0) {
          // Create user
          userModel = this.store.createRecord('user', {name: userName, score: 0, normalMode: true}).save();
        }
        // If is not a test make transition
        if (!isTest) {
          this.router.transitionTo('game', userName);
        }
        return userModel;
      })
      .catch(err => {
        return 'Join failed';
        // TODO: Notification error
      });
    }
    // Notification popup
    get(this, 'notifications').error(
      'You must set a user name',
      NOTIFICATION_OPTIONS,
    );
  };

}
