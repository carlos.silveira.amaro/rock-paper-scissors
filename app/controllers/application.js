import Controller from '@ember/controller';
import { action } from '@ember/object'
import { service } from "@ember/service";
import { not, equal } from '@ember/object/computed'
import { readOnly } from '@ember/object/computed'


export default class ApplicationController extends Controller {
  @service router;

  // Check if is not index route to show back button
  @equal('router.currentRouteName', 'index') isIndexRoute;
}
