import Controller from '@ember/controller';
import { action, get, set } from '@ember/object'
import { service } from "@ember/service";
import { isEmpty } from '@ember/utils'
import { alias } from '@ember/object/computed'
import { later } from '@ember/runloop'
import { notEmpty } from '@ember/object/computed'


export default class GameController extends Controller {
  @service store;

  // Normal mode options
  normalModeOptions = ['rock', 'paper', 'scissors'];

  // Full mode options
  fullModeOptions = ['rock', 'paper', 'scissors', 'lizard', 'spock'];

  lastBotPlay = null;
  userSelection = null;

  @notEmpty('lastBotPlay') hasLastBotPlay;
  @notEmpty('userSelection') hasUserSelection;

  @alias('model.firstObject') user;

  // Make play
  @action
  play(selection, isTest) {
    set(this, 'userSelection', selection);

    // Check mode
    if (this.user.normalMode) {
      // Normal mode

      // Get Options
      let botOption = this.normalModeOptions.filter((item) => { return item !== this.lastBotPlay});

      // Check Winner
      const context = this;
      return later(() => {
        // Choose option
        const numero = Math.floor(Math.random() * botOption.length);
        botOption = botOption[numero];
        if (!isTest) {
          set(context, 'lastBotPlay', botOption);
        } else {
          this.lastBotPlay = botOption;
        }
        if (selection === botOption) {
          // Draw
          if (!isTest) {
            set(context, 'result', 'Draw');
          } else {
            this.result = 'Draw';
          }
        } else {
          switch (selection) {
            case 'rock':
              return context.updateScore(botOption === 'scissors');
            case 'paper':
              return context.updateScore(botOption === 'rock');
            case 'scissors':
              return context.updateScore(botOption === 'paper');
            default:
              // TODO: Error Notification
          }
        }
      }, 1000);
    } else {
      // Sheldon mode
      // Get Options
      let botOption = this.fullModeOptions.filter((item) => { return item !== this.lastBotPlay});

      // Check Winner
      const context = this;
      return later(() => {
        // Choose option
        const numero = Math.floor(Math.random() * botOption.length);
        botOption = botOption[numero];
        if (!isTest) {
          set(context, 'lastBotPlay', botOption);
        } else {
          this.lastBotPlay = botOption;
        }
        if (selection === botOption) {
          // Draw
          if (!isTest) {
            set(context, 'result', 'Draw');
          } else {
            this.result = 'Draw';
          }
        } else {
          switch (selection) {
            case 'rock':
              return context.updateScore(botOption === 'scissors' || botOption == 'lizard');
            case 'paper':
              return context.updateScore(botOption === 'rock' || botOption == 'spock');
            case 'scissors':
              return context.updateScore(botOption === 'paper' || botOption == 'lizard');
            case 'lizard':
              return context.updateScore(botOption === 'spock' || botOption == 'paper');
            case 'spock':
              return context.updateScore(botOption === 'scissors' || botOption == 'rock');
            default:
              // TODO: Error Notification
          }
        }
      }, 1000);
    }
  };

  // Update user score action
  @action
  updateScore(winner) {
    if (isEmpty(this.user)) {
      return 'Empty user'
    } else {
      if (winner) {
        this.user.score++
        set(this, 'result', 'Winner');
        get(this, 'user').save();
      } else {
        set(this, 'result', 'Loser');
      }
      return 'Score Updated';
    }
  };

  // Toggles a boolean attribute
  @action
  togglePropertyAction(propertyName) {
    this.toggleProperty(propertyName)
  };

}
