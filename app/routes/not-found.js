import Route from '@ember/routing/route';

export default class IndexRoute extends Route {
  // Redirect not found routes to index
  beforeModel() {
    this.transitionTo('index');
  }
}
