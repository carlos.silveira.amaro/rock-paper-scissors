import Route from '@ember/routing/route';
import { service } from "@ember/service";


export default class GameRoute extends Route {
  @service store;


  model(params) {
    // Find user by name
    const {
      user_name
    } = params;
    return this.store.query('user', {
      filter: {
        name: params.user_name
      }
    });
  }
}
