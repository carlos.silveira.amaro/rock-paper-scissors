'use strict';

define("rock-paper-scissors/tests/helpers/index", ["exports", "ember-qunit"], function (_exports, _emberQunit) {
  "use strict";

  Object.defineProperty(_exports, "__esModule", {
    value: true
  });
  _exports.setupApplicationTest = setupApplicationTest;
  _exports.setupRenderingTest = setupRenderingTest;
  _exports.setupTest = setupTest;
  0; //eaimeta@70e063a35619d71f0,"ember-qunit"eaimeta@70e063a35619d71f

  // This file exists to provide wrappers around ember-qunit's / ember-mocha's
  // test setup functions. This way, you can easily extend the setup that is
  // needed per test type.
  function setupApplicationTest(hooks, options) {
    (0, _emberQunit.setupApplicationTest)(hooks, options); // Additional setup for application tests can be done here.
    //
    // For example, if you need an authenticated session for each
    // application test, you could do:
    //
    // hooks.beforeEach(async function () {
    //   await authenticateSession(); // ember-simple-auth
    // });
    //
    // This is also a good place to call test setup functions coming
    // from other addons:
    //
    // setupIntl(hooks); // ember-intl
    // setupMirage(hooks); // ember-cli-mirage
  }

  function setupRenderingTest(hooks, options) {
    (0, _emberQunit.setupRenderingTest)(hooks, options); // Additional setup for rendering tests can be done here.
  }

  function setupTest(hooks, options) {
    (0, _emberQunit.setupTest)(hooks, options); // Additional setup for unit tests can be done here.
  }
});
define("rock-paper-scissors/tests/test-helper", ["rock-paper-scissors/app", "rock-paper-scissors/config/environment", "qunit", "@ember/test-helpers", "qunit-dom", "ember-qunit"], function (_app, _environment, QUnit, _testHelpers, _qunitDom, _emberQunit) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"rock-paper-scissors/app",0,"rock-paper-scissors/config/environment",0,"qunit",0,"@ember/test-helpers",0,"qunit-dom",0,"ember-qunit"eaimeta@70e063a35619d71f

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));
  (0, _qunitDom.setup)(QUnit.assert);
  (0, _emberQunit.start)();
});
define("rock-paper-scissors/tests/unit/adapters/application-test", ["qunit", "rock-paper-scissors/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"rock-paper-scissors/tests/helpers"eaimeta@70e063a35619d71f

  (0, _qunit.module)('Unit | Adapter | application', function (hooks) {
    (0, _helpers.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let adapter = this.owner.lookup('adapter:application');
      assert.ok(adapter, 'Adapter loaded');
    });
  });
});
define("rock-paper-scissors/tests/unit/controllers/game-test", ["qunit", "rock-paper-scissors/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"rock-paper-scissors/tests/helpers"eaimeta@70e063a35619d71f

  (0, _qunit.module)('Unit | Controller | game', function (hooks) {
    (0, _helpers.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      const controller = this.owner.lookup('controller:game');
      assert.ok(controller, 'Game controller loaded');
    });
    (0, _qunit.test)('check lastBotPlay', async function (assert) {
      let controller = this.owner.lookup('controller:game');
      assert.equal(controller.lastBotPlay, null);
    });
    (0, _qunit.test)('check normal game options', async function (assert) {
      let controller = this.owner.lookup('controller:game');
      assert.equal(controller.normalModeOptions.length, 3);
    });
    (0, _qunit.test)('check full game options', async function (assert) {
      let controller = this.owner.lookup('controller:game');
      assert.equal(controller.fullModeOptions.length, 5);
    });
    (0, _qunit.test)('Play rock', function (assert) {
      let controller = this.owner.lookup('controller:game');
      let store = this.owner.lookup('service:store');
      controller.model = {};
      controller.model.firstObject = store.createRecord('user', {
        name: 'Test Name',
        score: 0,
        normalMode: true
      });
      let result = controller.play('rock', true);
      assert.equal(result, 0, 'Play done');
    });
    (0, _qunit.test)('Update Score', function (assert) {
      let controller = this.owner.lookup('controller:game');
      let store = this.owner.lookup('service:store');
      controller.model = {};
      controller.model.firstObject = store.createRecord('user', {
        name: 'Test Name',
        score: 0,
        normalMode: true
      });
      let result = controller.updateScore(true);
      assert.equal(result, 'Score Updated', 'Score Updated');
    });
  });
});
define("rock-paper-scissors/tests/unit/controllers/index-test", ["qunit", "rock-paper-scissors/tests/helpers", "@ember/utils"], function (_qunit, _helpers, _utils) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"rock-paper-scissors/tests/helpers",0,"@ember/utils"eaimeta@70e063a35619d71f

  (0, _qunit.module)('Unit | Controller | index', function (hooks) {
    (0, _helpers.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      const controller = this.owner.lookup('controller:index');
      assert.ok(controller, 'Index controller loaded');
    }); // Replace this with your real tests.

    (0, _qunit.test)('Join user', async function (assert) {
      let store = this.owner.lookup('service:router');
      let indexController = this.owner.lookup('controller:index');
      let model = indexController.join('Test name', true);
      await model;
      assert.ok(model._result !== 'Join failed', 'User created or loaded');
    });
  });
});
define("rock-paper-scissors/tests/unit/models/user-test", ["qunit", "rock-paper-scissors/tests/helpers", "@ember/utils"], function (_qunit, _helpers, _utils) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"rock-paper-scissors/tests/helpers",0,"@ember/utils"eaimeta@70e063a35619d71f

  (0, _qunit.module)('Unit | Model | user', function (hooks) {
    (0, _helpers.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('user', {
        name: 'Test Name',
        score: 0,
        normalMode: true
      });
      const modelAttributes = !(0, _utils.isEmpty)(model.name) && !(0, _utils.isEmpty)(model.name) && !(0, _utils.isEmpty)(model.normalMode);
      assert.ok(modelAttributes, 'User created with all attributes');
    });
  });
});
define("rock-paper-scissors/tests/unit/serializers/application-test", ["qunit", "rock-paper-scissors/tests/helpers"], function (_qunit, _helpers) {
  "use strict";

  0; //eaimeta@70e063a35619d71f0,"qunit",0,"rock-paper-scissors/tests/helpers"eaimeta@70e063a35619d71f

  (0, _qunit.module)('Unit | Serializer | application', function (hooks) {
    (0, _helpers.setupTest)(hooks); // Replace this with your real tests.

    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let serializer = store.serializerFor('application');
      assert.ok(serializer, 'Serializer loaded');
    });
    (0, _qunit.test)('it serializes records', function (assert) {
      let store = this.owner.lookup('service:store');
      let record = store.createRecord('user', {});
      let serializedRecord = record.serialize();
      assert.ok(serializedRecord, 'Record serialized');
    });
  });
});
define('rock-paper-scissors/config/environment', [], function() {
  var prefix = 'rock-paper-scissors';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(decodeURIComponent(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('rock-paper-scissors/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
